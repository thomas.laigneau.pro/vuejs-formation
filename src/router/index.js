import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Directives from '../views/Directives.vue'
import TwoWayBinding from '../views/TwoWayBinding.vue'
import TwoWayBindingExercise from '../views/TwoWayBindingExercise.vue'
import TestPage from '../views/TestPage.vue'
import FullExample from '../views/FullExample.vue'
import DirectivesExercise from '../views/DirectivesExercise.vue'
import StoreDemo from '../views/StoreDemo.vue'
import Axios from '../views/Axios.vue'
import ComponentToDo from '../views/ComponentToDo.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/component-example',
    name: 'FullExample',
    component: FullExample
  },
  {
    path: '/directives-exercise',
    name: 'FullExercise',
    component: DirectivesExercise
  },
  {
    path: '/directives',
    name: 'Directives',
    component: Directives
  },
  {
    path: '/two-way-binding',
    name: 'TwoWayBinding',
    component: TwoWayBinding
  },
  {
    path: '/two-way-binding-exercise',
    name: 'TwoWayBindingExercise',
    component: TwoWayBindingExercise
  },
  {
    path: '/test/:id', 
    name: 'testPage',
    component: TestPage 
  },
  {
    path: '/store-demo',
    name: 'StoreDemo',
    component: StoreDemo
  },
  {
    path: '/axios',
    name: 'AxiosDemo',
    component: Axios
  },
  {
    path: '/components-todo',
    name: 'ComponentToDo',
    component: ComponentToDo
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
